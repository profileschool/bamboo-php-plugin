# PHP Bamboo Plugin

Plugin for integrating [PHP][php] and [Composer][composer] with [Atlassian Bamboo][bamboo].
The plugin provides a number of tasks in Bamboo that you can use to create builds for your PHP project.

[php]: http://php.net/
[composer]: http://getcomposer.org
[bamboo]: http://www.atlassian.com/software/bamboo/overview

## Tasks

The PHP Bamboo plugin provides the following tasks in Bamboo:

* PHP (run PHP script)
* Composer (run Composer command)

## License

See LICENSE file for additional information.
