package com.profileschool.bamboo.plugins.composer;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.CommonTaskType;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.base.Preconditions;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import java.util.List;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import java.util.Map;

public class ComposerTaskType implements CommonTaskType
{
    public static final String CAPABILITY_PREFIX = CapabilityDefaultsHelper.CAPABILITY_BUILDER_PREFIX + ".composer";
    private final ProcessService processService;
    private final EnvironmentVariableAccessor environmentVariableAccessor;
    private final CapabilityContext capabilityContext;
    private final I18nResolver i18nResolver;

    public ComposerTaskType(final ProcessService processService,
                         final EnvironmentVariableAccessor environmentVariableAccessor,
                         final CapabilityContext capabilityContext,
                         final I18nResolver i18nResolver)
    {
        this.processService = processService;
        this.environmentVariableAccessor = environmentVariableAccessor;
        this.capabilityContext = capabilityContext;
        this.i18nResolver = i18nResolver;
    }

    @NotNull
    @Override
    public TaskResult execute(@NotNull CommonTaskContext taskContext) throws TaskException
    {
        final TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(taskContext);
        final BuildLogger buildLogger = taskContext.getBuildLogger();

        try
        {
            final ConfigurationMap configurationMap = taskContext.getConfigurationMap();

            final String runtime = configurationMap.get(ComposerConfigurator.RUNTIME);
            String composerPath = capabilityContext.getCapabilityValue(ComposerTaskType.CAPABILITY_PREFIX + "." + runtime);

            final String command = configurationMap.get(ComposerConfigurator.COMMAND);
            final String arguments = configurationMap.get(ComposerConfigurator.ARGUMENTS);

            Preconditions.checkState(StringUtils.isNotBlank(composerPath), i18nResolver.getText("composer.runtime.error.undefinedPath"));
            Preconditions.checkState(StringUtils.isNotBlank(command), i18nResolver.getText("composer.command.error.undefined"));

            final Map<String, String> extraEnvironmentVariables = environmentVariableAccessor.splitEnvironmentAssignments(
                    configurationMap.get(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES), false);

            final List<String> commandsList = Lists.newArrayList(composerPath, command);

            if (StringUtils.isNotBlank(arguments))
            {
                commandsList.add(arguments);
            }

            buildLogger.addBuildLogEntry("<composer-command>");
            buildLogger.addBuildLogEntry(Joiner.on(" ").join(commandsList));
            buildLogger.addBuildLogEntry("</composer-command>");

            return taskResultBuilder.checkReturnCode(processService.executeExternalProcess(taskContext, new ExternalProcessBuilder()
                    .command(commandsList)
                    .env(extraEnvironmentVariables)
                    .path(FilenameUtils.getFullPath(composerPath))
                    .workingDirectory(taskContext.getWorkingDirectory())))
                    .build();
        }
        catch (Exception e)
        {
            throw new TaskException("Failed to execute task", e);
        }

    }
}
