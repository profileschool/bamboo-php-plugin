package com.profileschool.bamboo.plugins.composer;

import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskRequirementSupport;
import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugins.git.GitCapabilityTypeModule;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.v2.build.agent.capability.Requirement;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementImpl;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.Map;
import java.util.Set;

public class ComposerConfigurator extends AbstractTaskConfigurator implements TaskRequirementSupport
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(ComposerConfigurator.class);
    private static final String CTX_UI_CONFIG_BEAN = "uiConfigBean";

    public static final String COMPOSER_DEFAULT_EXECUTABLE = "/usr/local/bin/composer";

    public static final String RUNTIME = "runtime";
    public static final String COMMAND = "command";
    public static final String ARGUMENTS = "arguments";

    protected I18nResolver i18nResolver;
    protected UIConfigSupport uiConfigSupport;

    protected static final Set<String> FIELDS_TO_COPY = ImmutableSet.<String>builder()
            .add(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY)
            .add(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES)
            .add(RUNTIME)
            .add(COMMAND)
            .add(ARGUMENTS)
            .build();

    protected static final Map<String, Object> DEFAULT_FIELD_VALUES = ImmutableMap.<String, Object>builder()
            .put(RUNTIME, COMPOSER_DEFAULT_EXECUTABLE)
            .put(COMMAND, "install")
            .build();

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);
        if (StringUtils.isBlank(params.getString(RUNTIME)))
        {
            errorCollection.addError(RUNTIME, i18nResolver.getText("composer.runtime.error.empty"));
        }

        if (StringUtils.isBlank(params.getString(COMMAND)))
        {
            errorCollection.addError(COMMAND, i18nResolver.getText("composer.command.error.empty"));
        }
    }

    @NotNull
    @Override
    public Set<Requirement> calculateRequirements(@NotNull TaskDefinition taskDefinition)
    {
        final String runtime = taskDefinition.getConfiguration().get(RUNTIME);
        Preconditions.checkNotNull(runtime, i18nResolver.getText("composer.runtime.error.empty"));

        return ImmutableSet.<Requirement>builder()
            .add(new RequirementImpl(GitCapabilityTypeModule.GIT_CAPABILITY, true, ".*", true))
            .add(new RequirementImpl(ComposerTaskType.CAPABILITY_PREFIX + "." + runtime, true, ".*", true))
            .build();
    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params, @Nullable TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> map = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(map, params, getFieldsToCopy());

        return map;
    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context)
    {
        super.populateContextForCreate(context);
        context.put(CTX_UI_CONFIG_BEAN, uiConfigSupport);
        context.putAll(getDefaultFieldValues());
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        context.put(CTX_UI_CONFIG_BEAN, uiConfigSupport);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, getFieldsToCopy());
    }

    public void setI18nResolver(I18nResolver i18nResolver)
    {
        this.i18nResolver = i18nResolver;
    }

    public void setUiConfigSupport(UIConfigSupport uiConfigSupport)
    {
        this.uiConfigSupport = uiConfigSupport;
    }

    @NotNull
    public Set<String> getFieldsToCopy()
    {
        return FIELDS_TO_COPY;
    }

    @NotNull
    public Map<String, Object> getDefaultFieldValues()
    {
        return DEFAULT_FIELD_VALUES;
    }
}
