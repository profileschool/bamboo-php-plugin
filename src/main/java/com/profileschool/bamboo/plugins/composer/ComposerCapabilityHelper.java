package com.profileschool.bamboo.plugins.composer;

import com.atlassian.bamboo.v2.build.agent.capability.AbstractFileCapabilityDefaultsHelper;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;
import org.jetbrains.annotations.NotNull;

public class ComposerCapabilityHelper extends AbstractFileCapabilityDefaultsHelper
{
    private static final String EXEC_NAME = "composer";

    @NotNull
    @Override
    protected String getExecutableName()
    {
        return EXEC_NAME;
    }

    @NotNull
    @Override
    protected String getCapabilityKey()
    {
        return CapabilityDefaultsHelper.CAPABILITY_BUILDER_PREFIX + EXEC_NAME;
    }
}
