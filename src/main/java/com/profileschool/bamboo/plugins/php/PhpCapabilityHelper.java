package com.profileschool.bamboo.plugins.php;

import com.atlassian.bamboo.v2.build.agent.capability.AbstractFileCapabilityDefaultsHelper;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;
import org.jetbrains.annotations.NotNull;

public class PhpCapabilityHelper extends AbstractFileCapabilityDefaultsHelper
{
    public static final String EXEC_NAME = "php";

    @NotNull
    @Override
    protected String getExecutableName()
    {
        return EXEC_NAME;
    }

    @NotNull
    @Override
    protected String getCapabilityKey()
    {
        return CapabilityDefaultsHelper.CAPABILITY_BUILDER_PREFIX + EXEC_NAME;
    }
}
