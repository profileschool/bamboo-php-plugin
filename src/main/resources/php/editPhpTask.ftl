[#-- @ftlvariable name="uiConfigBean" type="com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport" --]

[#assign addExecutableLink][@ui.displayAddExecutableInline executableKey='php' /][/#assign]
[@s.select cssClass="builderSelectWidget" labelKey='php.runtime' name='runtime'
list=uiConfigBean.getExecutableLabels('php')
extraUtility=addExecutableLink required=true /]

[@s.textfield labelKey='php.script' name='script' cssClass="long-field" required=true /]
[@s.textfield labelKey='php.arguments' name='arguments' cssClass="long-field" /]

[@ui.bambooSection titleKey='repository.advanced.option' collapsible=true
isCollapsed=!(environmentVariables?has_content || workingSubDirectory?has_content)]
    [@s.textfield labelKey='builder.common.env' name='environmentVariables' cssClass="long-field" /]
    [@s.textfield labelKey='builder.common.sub' name='workingSubDirectory' cssClass="long-field" /]
[/@ui.bambooSection]
