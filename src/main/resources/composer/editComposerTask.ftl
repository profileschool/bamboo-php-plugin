[#-- @ftlvariable name="uiConfigBean" type="com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport" --]

[#assign addExecutableLink][@ui.displayAddExecutableInline executableKey='composer' /][/#assign]

[@s.select
    name="runtime" labelKey="composer.runtime" cssClass="builderSelectWidget"
    list=uiConfigBean.getExecutableLabels("composer") extraUtility=addExecutableLink
    required=true
/]

[@s.textfield labelKey='composer.command' name='command' cssClass="long-field" required=true /]
[@s.textfield labelKey='composer.arguments' name='arguments' cssClass="long-field" /]

[@ui.bambooSection titleKey='repository.advanced.option' collapsible=true
isCollapsed=!(environmentVariables?has_content || workingSubDirectory?has_content)]
    [@s.textfield labelKey='builder.common.env' name='environmentVariables' cssClass="long-field" /]
    [@s.textfield labelKey='builder.common.sub' name='workingSubDirectory' cssClass="long-field" /]
[/@ui.bambooSection]
